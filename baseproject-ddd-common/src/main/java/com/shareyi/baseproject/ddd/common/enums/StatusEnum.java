package com.shareyi.baseproject.ddd.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * <p> 数据有效性；1：有效，-1：无效 </p>
 * `status` tinyint(4) DEFAULT '1' COMMENT '数据有效性；1：有效，-1：无效',
 *
 * @author david
 * @date 2018-08-12
 */
@AllArgsConstructor
@Getter
@ToString
public enum StatusEnum implements EnumCode<Integer> {

    /**
     * 有效数据
     */
    YES(1, "有效数据"),
    /**
     * 无效数据
     */
    NO(-1, "无效数据");

    /**
     * 编码
     */
    private final Integer code;
    /**
     * 描述信息
     */
    private final String desc;

}
