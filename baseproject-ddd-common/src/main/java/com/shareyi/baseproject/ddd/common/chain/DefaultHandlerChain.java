package com.shareyi.baseproject.ddd.common.chain;

import com.shareyi.baseproject.ddd.common.chain.handler.Handler;

import java.util.List;

/**
 * 默认的责任链处理器
 *
 * @author david
 * @date 2021-08-29 15:38
 */
public class DefaultHandlerChain<T> implements HandlerChain<T> {
    /**
     * 责任链列表
     */
    private final List<? extends Handler<T>> handlers;
    /**
     * 责任链当前下标
     */
    private int index = 0;

    /**
     * 构建器
     *
     * @param handlers
     */
    public DefaultHandlerChain(List<? extends Handler<T>> handlers) {
        this.handlers = handlers;
    }

    @Override
    public void handle(T t) throws Exception {
        Handler<T> next = this.getNext();
        if (next != null) {
            next.handle(t, this);
        }

    }

    /**
     * 获取下一个
     *
     * @return
     */
    protected Handler<T> getNext() {
        return this.handlers != null && this.index < this.handlers.size() ? this.handlers.get(this.index++) : null;
    }
}
