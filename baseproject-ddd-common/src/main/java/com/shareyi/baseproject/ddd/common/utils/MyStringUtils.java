package com.shareyi.baseproject.ddd.common.utils;

import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.UUID;

/**
 * String 工具类
 *
 * @author david
 * @date 2018/10/11
 */
public class MyStringUtils {

    /**
     * 将字符串split为List
     *
     * @param str       字符串
     * @param separator 裁剪码
     * @return 字符串list
     */
    public static List<String> splitToList(String str, String separator) {
        if (StringUtils.isEmpty(str)) {
            return Lists.newArrayListWithCapacity(0);
        }
        String[] strs = StringUtils.split(str, separator);
        return Lists.newArrayList(strs);
    }

    /**
     * 去掉空的字符串
     *
     * @param lineStrList
     * @param trim
     * @return
     */
    public static List<String> removeEmptyString(List<String> lineStrList, boolean trim) {
        if (CollectionUtils.isEmpty(lineStrList)) {
            return lineStrList;
        }
        List<String> trimedList = Lists.newArrayListWithCapacity(lineStrList.size());
        for (String line : lineStrList) {
            if (trim) {
                line = StringUtils.trim(line);
            }
            if (StringUtils.isEmpty(line)) {
                continue;
            }
            trimedList.add(line);
        }
        return trimedList;
    }

    /**
     * 生成UUID
     *
     * @return 无-的，且大写32位的UUID
     */
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "").toUpperCase();
    }
}
