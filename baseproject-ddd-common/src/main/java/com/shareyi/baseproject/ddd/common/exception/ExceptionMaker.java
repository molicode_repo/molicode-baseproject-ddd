package com.shareyi.baseproject.ddd.common.exception;

import com.shareyi.baseproject.ddd.common.enums.ResultCodeEnum;

/**
 * 异常构建器，用于抛出异常
 *
 * @author david
 * @date 2018/8/21
 */
public class ExceptionMaker {

    /**
     * 构建异常
     *
     * @param resultCodeEnum
     * @return
     */
    public static BizException buildException(ResultCodeEnum resultCodeEnum) {
        return new BizException(resultCodeEnum);
    }

    /**
     * 构建异常
     *
     * @param message
     * @param resultCodeEnum
     * @return
     */
    public static BizException buildException(String message, ResultCodeEnum resultCodeEnum) {
        return new BizException(message, resultCodeEnum.codeString());
    }


}
